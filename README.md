# Antipandemic posters

<strong>[EN]</strong>

"Antipandemic Posters" is a series of signs that are ready to be printed and pasted wherever it's necessary to recall certain rules necessary to stop viral epidemics and pandemics. They have the size of an A4 paper sheet and are black and white with minimal printed surfaces, so to avoid ink waste. The drawings in them can be colour customized (by painting them by hand or on the computer, for instance) as needed. They're already translated in several languages (English, French, Italian, Spanish).

Antipandemic Posters is a project initiated by Ariel Martín Pérez in April 2020. The typeface used in them is Work Sans by Wei Huang, you can check it out here (https://github.com/weiweihuanghuang/Work-Sans). This project has been published under a Creative Commons Attribution-NonCommercial-ShareAlike licence, which means that you can modify and distribute it (always citing the original author), but derived projects must be non-commercial and published under the same licence. See details of the licence below.

<strong>[FR]</strong>

"Antipandemic Posters" est une série de panneaux prêts à être imprimés et collés partout où cela est nécessaire pour rappeler certaines règles nécessaires pour arrêter les épidémies virales et les pandémies. Ils ont la taille d'une feuille de papier A4 et ils sont en noir et blanc avec des surfaces imprimées minimales, afin d'éviter le gaspillage. Les dessins qu'ils contiennent peuvent être personnalisés en couleur (en les peignant à la main ou sur l'ordinateur, par exemple) selon les besoins de leur situation. Ils sont déjà traduits en plusieurs langues (anglais, français, italien, espagnol).

Antipandemic Posters est un projet initié par Ariel Martín Pérez en avril 2020. Le caractère typographique utilisé sur eux est Work Sans par Wei Huang, vous pouvez le télécharger sur ce lien (https://github.com/weiweihuanghuang/Work-Sans). Ce projet a été publié sur une licence Creative Commons "Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions", ce qui veut dire que vous avez le droit de le modifier et de le distribuer (toujours en citant son auteur), mais tout projet dérivé doit être non-commercial et publié sous la même licence. Voir détails de la licence sur la fin de ce document.

<strong>[ES]</strong>

Los "Antipandemic Posters" son una serie de letreros listos para ser impresos y pegados donde sea necesario para recordar ciertas reglas necesarias para detener las epidemias y pandemias virales. Tienen el tamaño de una hoja de papel A4 y son en blanco y negro con el mínimo de superficies impresas, para evitar los derroches de tinta. Los dibujos en ellos pueden ser personalizados con colores (pintándolos a mano o con el ordenador, por ejemplo) según sea necesario. Ya se encuentran traducidos a varios idiomas (español, francés, inglés, italiano).

Antipandemic Posters es un proyecto creado por Ariel Martín Pérez en abril de 2020. El tipo de letra utilizado en ellos es Work Sans por Wei Huang, puedes descargarlo en el enlace siguiente (https://github.com/weiweihuanghuang/Work-Sans). Este proyecto ha sido publicado bajo una licencia Creative Commons "Atribución-NoComercial-CompartirIgual", lo cual quiere decir que tienes el derecho de modificarlo y distribuirlo (siempre que se cite su autor), pero que todo proyecto derivado debe ser no-comercial y publicado bajo la misma licencia. Los detalles de la licencia se pueden ver al final del presente documento.

## Specimen

![specimen1](documentation/specimen/antipandemic-specimen-01.png)

![specimen2](documentation/specimen/antipandemic-specimen-02.png)

![specimen3](documentation/specimen/antipandemic-specimen-03.png)

## License

Antipandemic posters is licensed under the Attribution-NonCommercial-ShareAlike 4.0 International.
This license is copied on this repository, and is also available with a FAQ at
https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en

## Repository Layout

This poster repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/unified-font-repository/Unified-Font-Repository
